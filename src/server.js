import express from 'express';
import path from 'path';
import bodyParser from 'body-parser';

import React from 'react';
import { renderToString } from 'react-dom/server';
import { StaticRouter } from 'react-router';
import { renderRoutes, matchRoutes } from 'react-router-config';
import { Helmet } from 'react-helmet';
import { Provider } from 'react-redux';

import createStore from './app/bootstrap/redux/createStore';
import routes from './app/bootstrap/routes';
import template from './app/bootstrap/template.js';
import assets from './manifest.json';

import { fetchUser } from './app/components/features/Auth/redux/actions';

const PORT = process.env.PORT || 2002;
const PROD = process.env.MODE === 'production';
const server = express();
const store = createStore();

server.use(bodyParser.urlencoded({ extended: true }));
server.use(bodyParser.json());
server.use(express.static(path.join(__dirname, "../build")));
server.get('*', (request, response) => {
  store.dispatch(fetchUser(request)).then(() => {
    const branch = matchRoutes(routes, request.url);
    const promises = branch.map(({ route, match }) => {
      return route.component.fetchData instanceof Function ? route.component.fetchData(store, request) : Promise.resolve(null);
    });
  
    return Promise.all(promises).then(data => {
      const state = store.getState();
      const context = {};
      const markup = renderToString(
        <Provider store={store}>
          <StaticRouter location={request.url} context={context}>
            {renderRoutes(routes)}
          </StaticRouter>
        </Provider>
      );
  
      if (context.url) response.redirect(301, context.url);
      else response.send(template({
        state,
        helmet: Helmet.renderStatic(),
        assets: {
          css: PROD ? `/dist/${assets['bundle.css']}` : 'http://localhost:8080/bundle.css',
          js: PROD ? `/dist/${assets['bundle.js']}` : 'http://localhost:8080/bundle.js'
        },
        markup,
        isProduction: PROD
      }));
    }).catch(err => {
      console.log(err);
    });
  });
});

server.listen(PORT, () => {
  console.log(`Server started at ${PORT}`);
});
