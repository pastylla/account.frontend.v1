import 'slick-carousel/slick/slick.css';
import './normalize.css';
import './fonts.css';
import './colors.css';
import './animations.css';

import "./style.css";
import './tablet.css';
import './mobile.css';
import './widescreen.css';

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { renderRoutes } from 'react-router-config';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

import { PROJECT_NAME } from '@bootstrap/constants';

class Boot extends Component {
  static propTypes = {
    route: PropTypes.object,
    user: PropTypes.object
  };

  render() {
    const { user, location, route } = this.props;
    const helmet = {
      titleTemplate: `%s | ${PROJECT_NAME}`,
      titleAttributes: {
        itemprop: "name",
        lang: "en"
      }
    };

    const isAuthChildComponent = location.pathname.includes('auth');

    if (!user && !isAuthChildComponent) return <Redirect to='/auth/login' />;
    else if (user && isAuthChildComponent) return <Redirect to='/' />;

    return (
      <React.Fragment>
        <Helmet {...helmet} />
        {renderRoutes(route.routes)}
      </React.Fragment>
    );
  }
}

const mapDispatchToProps = ({ auth }) => ({ user: auth.user });

export default connect(mapDispatchToProps)(Boot);
