import Entrypoint from '@app';

import NotFoundPage from '@features/NotFoundPage';

// Auth Flow
import Auth from '@features/Auth';

import Signup from '@features/Signup';
import Login from "@features/Login";
import Restore from "@features/Restore";

// Create Flow
import TopicList from '@features/TopicList';

import withTopicList from '@features/TopicList/hoc/withTopicList';

// Presentation flow
import Pastylla from '@features/Pastylla';
import QuestionList from '@features/QuestionList';
import Design from '@features/Design';
import Download from '@features/Download';

import withPastylla from '@features/Pastylla/hoc/withPastylla';

export default [
  {
    component: Entrypoint,
    routes: [
      { path: '/', component: TopicList, exact: true },
      { path: '/topic', component: TopicList, exact: true },
      { path: '/topic/:topicId', component: withTopicList(QuestionList), exact: true },
      { 
        path: '/auth', 
        component: Auth,
        routes: [
          { path: '/auth/login', component: Login },
          { path: '/auth/signup', component: Signup },
          { path: '/auth/restore', component: Restore }
        ]
      },
      { 
        path: '/:pastyllaId', 
        component: Pastylla,
        routes: [
          { path: '/:pastyllaId/questions', component: withPastylla(QuestionList), exact: true },
          { path: '/:pastyllaId/design', component: Design, exact: true },
          { path: '/:pastyllaId/download', component: Download, exact: true }
        ]
      },
      {
        path: '*',
        component: NotFoundPage,
        exact: true
      }
    ]
  }
];
