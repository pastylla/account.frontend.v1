import { routerReducer } from 'react-router-redux';
import { compose, createStore, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';

import auth from '@features/Auth/redux/reducer';
import topiclist from '@features/TopicList/redux/reducer';
import pastylla from '@features/Pastylla/redux/reducer';
import design from '@features/Design/redux/reducer';

export default (initialState = {}) => {
  const preloadedState = typeof(window) !== 'undefined' ? window.__STATE__ : initialState;
  const rootReducer = combineReducers({
    router: routerReducer,

    auth,

    topiclist,
    pastylla,
    design
  });

  return createStore(rootReducer, preloadedState, compose(applyMiddleware(thunk)));
};
