import 'isomorphic-fetch';

const error = (error) => {
  return console.log(error);
}

export const fetchData = (url, callback,  method, body, request, isArchive) => (dispatch) => {
  const headers = request ? request.headers : {};
  const token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1Y2ZjZjQxNzBjMjFkZTAzMTg0OWE2MzgiLCJlbWFpbCI6InRlc3RAcGFzdHlsbGEuaW8iLCJuYW1lIjoiVGVzdCBVc2VyIiwiaWF0IjoxNTYwMTA3MTI2fQ.NXoE5I197ngWzR3VhsbMCvDBQbneV10p6cc-lccWr70";

  headers["Authorization"] = `Bearer ${token}`;

  if (!isArchive) headers["Content-Type"] = "application/json";

  return fetch(url, {
    method: method ? method : 'GET',
    body,
    headers
  })
  .then(res => {
    if (res.status !== 200) return error(data);
    else return callback(res, dispatch);
  });
}
