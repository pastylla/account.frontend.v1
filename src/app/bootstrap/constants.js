/*
  Identifies API location url
*/
const identifyApiUrl = () => {
  // Url where project located. 
  const backendLocation = process.env.ACCOUNT_BACKEND ? process.env.ACCOUNT_BACKEND : 'http://pastylla.io';
  // Url where API located 
  const apiLocation = `api/v1`;

  /*
    If request to API made from frontend, make it relative to window.location
    if request was made from bakend, make it absolete to project location.
  */
  if (typeof (window) !== 'undefined') {
    return `\/${apiLocation}`;
  }
  else {
    return `${backendLocation}\/${apiLocation}`;
  }
}

const identifyAuthUrl = () => {
  const backendLocation = process.env.AUTH_BACKEND ? process.env.AUTH_BACKEND : 'http://localhost:42001/api/v1';
  const apiLocation = 'api/v1';

  if (typeof (window) !== 'undefined') {
    return `http://localhost:42001/${apiLocation}`;
  } else {
    return `${backendLocation}\/${apiLocation}`;
  }
}


export const PROJECT_NAME = 'Pastylla';
export const API_URL = identifyApiUrl();
export const AUTH_URL = identifyAuthUrl();
