export default ({ state, helmet, assets, markup, isProduction }) => {
  return `
    <!DOCTYPE html>
    <html lang="ru">
      <head>
        ${helmet.title.toString()}
        <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="apple-touch-icon" sizes="57x57" href="/assets/images/share/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="/assets/images/share/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/assets/images/share/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="/assets/images/share/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/assets/images/share/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="/assets/images/share/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="/assets/images/share/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="/assets/images/share/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="/assets/images/share/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="/assets/images/share/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/assets/images/share/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="/assets/images/share/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/assets/images/share/favicon-16x16.png">
        <link rel="manifest" href="/assets/images/share/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="/assets/images/share/ms-icon-144x144.png">
        <meta name="theme-color" content="#2e2727">
        ${helmet.meta.toString()}
        ${helmet.link.toString()}
        <link href="https://fonts.googleapis.com/css?family=Montserrat:500,600&amp;subset=cyrillic-ext" rel="stylesheet"> 
        <link rel='stylesheet' href='${assets.css}' />
        ${isProduction ? (`
          <!-- Google Tag Manager -->

          <!-- End Google Tag Manager -->
        `) : ''}
      </head>
      <body>
        ${isProduction ? (
          `<!-- Google Tag Manager (noscript) -->
          
          <!-- End Google Tag Manager (noscript) -->`
        ) : ''}
        <div id='boot'>${markup}</div>

        <script>window.__STATE__ = ${JSON.stringify(state)}</script>
        <script src='${assets.js}'></script>

        ${isProduction ? (
          `<!-- Yandex.Metrika counter -->
          
          <!-- /Yandex.Metrika counter -->`
        ) : ''}
      </body>
    </html>
  `;
};
