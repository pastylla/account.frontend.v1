import JSZip from 'jszip';

export default (stream, isDataUrl) => {
  const zip = new JSZip(); 

  return zip.loadAsync(stream, { base64: !!isDataUrl }).then(archive => {
    const promises = [];
    const _structure = [
      'topic.json', // Topic data with questions
      'meta.json', // Meta data of pastylla
      'content.json', // Content with variables
      'template.json' // Template structure
    ];

    _structure.map(_file => promises.push(archive.file(_file).async('text')));

    return Promise.all(promises);
  });
}