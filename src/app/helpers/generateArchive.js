import JSZip from 'jszip';

export default (stream, { topic, content }) => {
  const archive = new JSZip();
  
  return archive.loadAsync(stream).then(_pastylla => {
    const meta = {
      title: topic.title,
      description: topic.description,
      version: "0.0.1",
      author: "Test User <test@pastylla.io>",
      generator: "Pastylla",
      created_at: Date.now(),
      updated_at: Date.now()
    };

    

    _pastylla.file('topic.json', JSON.stringify(topic));
    _pastylla.file('content.json', JSON.stringify(content));
    _pastylla.file('meta.json', JSON.stringify(meta));
    _pastylla.folder('images');

    return _pastylla.generateAsync({ type: 'blob' });
  });
}