import "./style.css";
import "./mobile.css";
import "./tablet.css";
import "./widescreen.css";

import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { restore } from '@features/Auth/redux/actions';

import AuthForm from "@containers/AuthForm";

class Restore extends Component {
  static propTypes = {
    restore: PropTypes.func,
  };

  render() {
    const params = {
      fields: [
        {
          id: "mail",
          type: "text",
          label: "Е-mail",
          message: "Введите вашу почту",
          r: e => this.mail = e,
          error: { message: "try something else", show: false }
        }
      ],
      links: [
        { name:"login", href:"/auth/login" },
        { name:"restore", href:"/auth/restore" },
        { name:"signup", href:"/auth/signup" }
      ],
      button: 'Сбросить пароль',
      r: (id, e) => this[id] = e,
      onClick: this.props.restore
    };

    return <AuthForm {...params} />;
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({ restore }, dispatch);

export default connect(null, mapDispatchToProps)(Restore);
