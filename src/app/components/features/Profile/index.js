import "./style.css";
import "./widescreen.css";
import "./tablet.css";
import "./mobile.css";

import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

import Drawer from "@containers/Drawer";

import Title from "@ui/Title";
import Text from "@ui/Text";
import Label from "@ui/Label";
import Notification from "@ui/Notification";
import Button from "@ui/Button";
import Input from "@ui/Input";
import Tariff from "../../ui/Tariff";

class Profile extends Component {
  static propTypes = {
    user: PropTypes.object
  };

  constructor() {
    super();

    this.state = {
      isBodyOverflow: false,
      isProfileOpen: false,
      isProfileEditOpen: false,
      isNotificationsOpen: false
    };
  }

  handleBodyOverflow = () => {
    const {
      isBodyOverflow,
      isProfileOpen,
      isProfileEditOpen,
      isNotificationsOpen
    } = this.state;
    if (!isBodyOverflow && (!isProfileOpen || !isProfileEditOpen || !isNotificationsOpen)) {
      document.body.style.overflowY = "hidden";
      this.setState({ isBodyOverflow: true });
    } else {
      document.body.style.overflowY = "auto";
      this.setState({ isBodyOverflow: false });
    }
  };

  handleProfileToggle = () => {
    const { isProfileOpen } = this.state;
    this.handleBodyOverflow();
    this.setState({ isProfileOpen: !isProfileOpen });
  };
  handleProfileEditToggle = () => {
    const { isProfileEditOpen } = this.state;
    this.handleBodyOverflow();
    this.setState({ isProfileEditOpen: !isProfileEditOpen });
  };
  handleNotificationToggle = () => {
    const { isNotificationsOpen } = this.state;
    this.handleBodyOverflow();
    this.setState({ isNotificationsOpen: !isNotificationsOpen });
  };

  renderPhoto() {
    return (
      <div onClick={this.handleProfileToggle} className="profile__photo" />
    );
  }

  renderProfileDrawer() {
    const { isProfileOpen } = this.state;
    return (
      <Drawer
        open={isProfileOpen}
        onClick={this.handleProfileToggle}
        title="Обо мне"
        action={{ title: "Редактировать", func: this.handleProfileEditToggle }}
      >
        <div className="profile__drawer__section profile__drawer__section-avatar" />
        <div className="profile__drawer__section profile__drawer__section-name">
          <Label title="Тебя зовут">
            <Title level={5} template="white">
              Роман К.
            </Title>
          </Label>
        </div>
        <div className="profile__drawer__section profile__drawer__section-mail">
          <Label title="Почта для связи">
            <Title level={5} template="white">
              romank@gmail.com
            </Title>
          </Label>
        </div>
        <div className="profile__drawer__section profile__drawer__section-facebook">
          <Label title="Facebook">
            <Title level={5} template="white">
              fb.com/romank
            </Title>
          </Label>
        </div>
        <div className="profile__drawer__section profile__drawer__section-vk">
          <Label title="ВКонтакте">
            <Title level={5} template="white">
              vk.com/romank
            </Title>
          </Label>
        </div>
        <div className="profile__drawer__section profile__drawer__section-google">
          <Label
            title="Google"
            additional="Этот профиль используется для входа на сайт"
          >
            <Title level={5} template="white">
              Роман К
            </Title>
          </Label>
        </div>
        <div className="profile__drawer__section profile__drawer__section-tariff">
          <Tariff edit={false} />
        </div>
      </Drawer>
    );
  }

  renderProfileEditDrawer() {
    const { isProfileEditOpen } = this.state;
    return (
      <Drawer
        open={isProfileEditOpen}
        onClick={this.handleProfileEditToggle}
        title="Отменить"
        action={{
          title: "готово",
          func: () => {
            console.log("ready");
          }
        }}
      >
        <div className="profile__edit__section profile__edit__section-name">
          <Label title="Тебя зовут">
            <Input
              template="white"
              value="Роман К."
              error={{ show: false }}
              r={() => {}}
              modifiers={["profile"]}
            />
          </Label>
        </div>
        <div className="profile__edit__section profile__edit__section-mail">
          <Label title="Почта для связи">
            <Input
              template="white"
              value="romank@gmail.com"
              error={{ show: false }}
              r={() => {}}
              modifiers={["profile"]}
            />
          </Label>
        </div>
        <div className="profile__edit__section profile__edit__section-tariff">
          <Tariff edit={true} />
        </div>
      </Drawer>
    );
  }

  renderNotifications() {
    const { isNotificationsOpen } = this.state;
    return (
      <Drawer
        open={isNotificationsOpen}
        onClick={this.handleNotificationToggle}
        title="Уведомления"
      >
        <div className="profile__notifications__section">
          <Notification
            date="10.15"
            title="Ваш баланс пополнен"
            text="На вашем счету 100 презентаций"
            unread={true}
            link="/create/topic"
          />
        </div>
        <div className="profile__notifications__section">
          <Notification
            date="14 мая"
            title="Доступен экспорт в PPTX и HTML"
            text="Теперь доступны новые форматы для экспорта - PPTX и HTML"
            unread={true}
            link="/create/topic"
          />
        </div>
        <div className="profile__notifications__section">
          <Notification
            date="13 мая"
            title="Ваш баланс пополнен"
            text="На вашем счету 100 презентаций"
          />
        </div>
        <div className="profile__notifications__section">
          <Notification
            date="14 мая"
            title="Доступен экспорт в PPTX и HTML"
            text="Теперь доступны новые форматы для экспорта - PPTX и HTML"
            link="/create/topic"
          />
        </div>
      </Drawer>
    );
  }

  render() {
    const { user } = this.props;

    return (
      <div className={`profile`}>
        <div className="profile__section profile__section-photo">
          {this.renderPhoto()}
        </div>
        <div className="profile__section profile__section-drawer">
          {this.renderProfileDrawer()}
        </div>
        <div className="profile__section profile__section-drawer">
          {this.renderProfileEditDrawer()}
        </div>
        <div className="profile__section profile__section-notifications">
          {this.renderNotifications()}
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ auth }) => ({ user: auth.user });

export default connect(mapStateToProps)(Profile);
