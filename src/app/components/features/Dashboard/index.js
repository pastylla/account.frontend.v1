import "./style.css";
import './mobile.css';
import './tablet.css';
import './widescreen.css';

import React, { Component } from "react";
import PropTypes from 'prop-types';
import { Helmet } from "react-helmet";
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';

import TopicList from "@features/topiclist";

class Dashboard extends Component {
  static propTypes = {
    user: PropTypes.object
  }
  configureHelmet() {
    const params = {
      title: "Pastylla",
      meta: [
        {
          name: "description",
          content: "pastylla website"
        }
      ]
    };

    return <Helmet {...params} />;
  }

  render() {
    const { user } = this.props;

    if (!user) return <Redirect to='/auth/login' />;
    
    return (
      <div className='dashboard'>
        <TopicList />
      </div>
    );
  }
}

const mapStateToProps = ({ auth }) => ({ user: auth.user });

export default connect(mapStateToProps)(Dashboard);

