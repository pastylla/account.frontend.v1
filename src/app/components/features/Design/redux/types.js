import { PROJECT_NAME } from '@bootstrap/constants';

export const COMPONENT_DESIGN_FETCH = `@@${PROJECT_NAME}/component/design/FETCH`;
