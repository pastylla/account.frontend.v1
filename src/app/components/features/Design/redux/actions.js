import 'isomorphic-fetch';

import { API_URL } from '@bootstrap/constants';

import { fetchData } from '@bootstrap/redux/actions';

export const fetchDesign = (designId) => fetchData(
  `${API_URL}/design/${designId}`, 
  res => res.blob(), 
  'GET', 
  null,
  null,
  true
);
