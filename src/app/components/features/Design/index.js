import './style.css';
import './widescreen.css';
import './tablet.css';
import './mobile.css';

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
 
import { fetchDesign } from '@features/Design/redux/actions';
import { downloadJpg } from '@features/Download/redux/actions';

import withPastylla from '@features/Pastylla/hoc/withPastylla';

import Layout from '@containers/Layout';
import Actionbar from '@containers/Actionbar';
import Sidebar from '@containers/Sidebar';
import Slide from '@containers/Slide';

class Design extends Component {
  static propTypes = {
    id: PropTypes.string,
    topic: PropTypes.object,
    updatePastylla: PropTypes.func,
    downloadJpg: PropTypes.func,
    fetchDesign: PropTypes.func,
    pastylla: PropTypes.object,
    stream: PropTypes.object,
    design: PropTypes.array
  };

  constructor() {
    super();

    this.state = { images: null }
  }

  componentDidMount() {
    const { downloadJpg, stream } = this.props;

    const _form = new FormData();
    
    _form.append('file', stream, `${Date.now()}.pastylla`);

    downloadJpg(_form).then(data => {
      this.setState({ images: data });
    });
  }

  renderActionbar() {
    const params = {
      items: [
        { _id: '1somes', icon: 'burger', template:"transparent" },
        { _id: '2somes', icon: 'play' },
        { _id: '3somes', icon: 'share' }
      ]
    };

    return <Actionbar {...params} />;
  }

  renderSidebar() {
    const { id } = this.props;
    const params = {
      template: 'selectDesign',
      previousPageLink: {
        title: 'К списку вопросов',
        href: `/${id}/questions`
      }
    };

    return <Sidebar {...params} />;
  }

  render() {
    const { pastylla } = this.props;
    const { template, topic } = pastylla;
    const { structure } = template;
    const { images } = this.state;

    return (
      <Layout>
        {this.renderActionbar()}
        <div className='design'>
          {images && images.map((slide, i) => {
            const _type = topic.structure[i];
            const _templates = [];

            for (let i = 0; i < structure.length; i++) {
              const _tpl = structure[i];

              if (_tpl.type === _type) {
                _templates.push({
                  template: _tpl
                });
              }
            }

            console.log(_templates);

            return (
              <div className='design__section'>
                <Slide preview={`data:image/jpg;base64, ${slide}`} templates={_templates} />
              </div>  
            );
          })}            
        </div>
        {this.renderSidebar()}
      </Layout>
    );
  }
}

const mapStateToProps = ({ design }) => ({ design: design.items });
const mapDispatchToProps = dispatch => bindActionCreators({ fetchDesign, downloadJpg }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(withPastylla(Design));