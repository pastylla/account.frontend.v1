import './style.css';
import './widescreen.css';
import './tablet.css';
import './mobile.css';

import React from 'react';
import { Redirect } from 'react-router-dom';
import { renderRoutes } from 'react-router-config';

export default ({ location, route }) => {
  if (location.pathname === '/create') return <Redirect to='/create/topic' />;
  else return renderRoutes(route.routes);
}