import { PROJECT_NAME } from '@bootstrap/constants';

export const COMPONENT_PASTYLLA_FETCH = `@@${PROJECT_NAME}/component/pastylla/FETCH`;
