import 'isomorphic-fetch';

import { API_URL } from '@bootstrap/constants';
import { COMPONENT_PASTYLLA_FETCH } from './types';

import { fetchData } from '@bootstrap/redux/actions';

const callback = (data, dispatch) => {
  data.json().then(_json => {
    // TODO: Set proper error handling
    if(_json.error) return console.log(_json.error);

    return dispatch({ type: COMPONENT_PASTYLLA_FETCH, items: _json });
  });
}


export const fetchPastylla = (pastyllaId) => fetchData(
  `${API_URL}/pastylla/${pastyllaId}`, 
  res => res.blob(), 
  'GET', 
  null,
  null,
  true
);

export const fetchPastyllaList = () => fetchData(
  `${API_URL}/pastylla`,
  callback,
  'GET'
);

export const createPastylla = (data) => fetchData(
  `${API_URL}/pastylla`, 
  res => res.json(),
  'POST', 
  data,
  null,
  true
);

export const updatePastylla = (data, pastyllaId) => fetchData(
  `${API_URL}/pastylla/${pastyllaId}`, 
  res => res.json(), 
  'PUT', 
  data,
  null,
  true
);