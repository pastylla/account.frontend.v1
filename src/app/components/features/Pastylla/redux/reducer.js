import { COMPONENT_PASTYLLA_FETCH } from './types';

const initialState = {
  items: []
}

export default (state = initialState, action) => {
  switch (action.type) {
    case COMPONENT_PASTYLLA_FETCH:
      return Object.assign({}, state, { items: action.items });
    default:
      return state;
  }
};
