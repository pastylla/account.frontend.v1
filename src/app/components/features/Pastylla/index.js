import './style.css';
import './widescreen.css';
import './tablet.css';
import './mobile.css';

import React from 'react';
import { Redirect } from 'react-router-dom';
import { renderRoutes } from 'react-router-config';

export default ({ location, route, match }) => {
  const { params } = match;
  const { pastyllaId } = params;

  if (location.pathname === `/${pastyllaId}`) return <Redirect to={`/${pastyllaId}/questions`} />;
  else return renderRoutes(route.routes);
}