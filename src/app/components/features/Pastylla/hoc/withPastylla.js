import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import readArchive from '@helpers/readArchive';
import { fetchPastylla } from '../redux/actions';


export default function (WrappedComponent) {
  const mapDispatchToProps = dispatch => bindActionCreators({ fetchPastylla }, dispatch);

  return connect(null, mapDispatchToProps)(class WithPastylla extends Component {
    static propTypes = {
      match: PropTypes.object,
      fetchPastylla: PropTypes.func
    };

    constructor(props) {
      super();

      const { match } = props;
      const { params } = match;
      const { pastyllaId } = params;

      this.state = { _id: pastyllaId };
    }

    componentWillMount() {
      const { fetchPastylla } = this.props;
      const { _id } = this.state;

      if (_id && typeof(window) !== 'undefined') {
        fetchPastylla(_id)
          .then(data => {
            this.stream = data;
  
            return readArchive(data)
          })
          .then(data => {
            const [ topic, meta, content, template ] = data;
            const pastylla = {
              topic: JSON.parse(topic),
              meta: JSON.parse(meta),
              content: JSON.parse(content),
              template: JSON.parse(template)
            };

            this.setState({ pastylla });
          });
      }
    }

    render() {
      const { pastylla, _id } = this.state;

      if (pastylla) {
        const { topic } = pastylla;

        return <WrappedComponent id={_id} topic={topic} stream={this.stream} pastylla={pastylla} {...this.props} />;
      }
      
      return <div></div>;
    }
  })
}