import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { fetchTopicList } from '../redux/actions';

export default function (WrappedComponent) {
  const mapStateToProps = ({ topiclist }) => ({ topicList: topiclist.items });
  const mapDispatchToProps = dispatch => bindActionCreators({ fetchTopicList }, dispatch);

  return connect(mapStateToProps, mapDispatchToProps)(class WithTopicList extends Component {
    static propTypes = {
      topicList: PropTypes.array,
      fetchTopicList: PropTypes.func
    };

    static fetchData = (store, request) => {
      return store.dispatch(fetchTopicList(request));
    }

    componentDidMount() {
      this.props.fetchTopicList();
    }

    render() {
      const { topicList, match } = this.props;
      const { params } = match;
      const { topicId } = params;

      if (topicList) {
        const topic = topicList.find(item => item._id === topicId);

        return <WrappedComponent topic={topic} {...this.props} />;
      } else {
        return <div></div>;
      }
    }
  });
}