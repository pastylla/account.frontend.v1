import './style.css';
import './widescreen.css';
import './tablet.css';
import './mobile.css';

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import withTopicList from './hoc/withTopicList';
import { fetchPastyllaList } from '@features/Pastylla/redux/actions';

import Layout from '@containers/Layout';
import Actionbar from '@containers/Actionbar';
import Sidebar from '@containers/Sidebar';
import Roadmap from '@containers/Roadmap';
import Topic from '@containers/Topic';
import Preview from '@containers/Preview';

import Title from '@ui/Title';
import Slider from '../../ui/Slider';
import Icon from '@ui/Icon';

class TopicList extends Component {
  static propTypes = {
    location: PropTypes.object,
    topicList: PropTypes.array,
    fetchTopicList: PropTypes.func,
    pastylla: PropTypes.array
  };

  constructor() {
    super();

    this.state = { loading: true };
  }

  componentDidMount() {
    const { fetchPastyllaList } = this.props;

    fetchPastyllaList().then(data => {
      this.setState({ loading: false });
    });
  }

  renderActionbar() {
    const params = {
      items: [
        { _id: '1somes', icon: 'burger', template:"transparent" },
        { _id: '2somes', icon: 'play' },
        { _id: '3somes', icon: 'share' }
      ]
    };

    return <Actionbar {...params} />;
  }

  renderSidebar() {
    const { loading } = this.state;
    const { pastylla } = this.props;
    const showFaq = !loading && !pastylla.length;
    const roadmap = [
      { title: 'Выбор темы', description: 'Какие задачи вы хотите решить с помощью презентации', active: true },
      { title: 'Ответы на вопросы', description: 'Заполнение анкеты происходит в режиме вопрос-ответ' },
      { title: 'Выбор дизайна', description: 'Выберите дизайн и скачайте презентацию' }
    ];

    return (
      <Sidebar template='topiclist'>
        {showFaq && <Title level={5} template='grey'>FAQ</Title>}
        {showFaq && <Title level={3} template='black'>Любую презентацию можно создать в три простых шага</Title>}
        {showFaq && <Roadmap items={roadmap} />}
        {!showFaq && <Title level={3} template='black'>Мои презентации</Title>}
        {!showFaq && <Title level={5} template='grey'>Последние созданные</Title>}
        {!showFaq && (
          <div className='topiclist__pastylla'>
            {pastylla.map(item => {
              return (
                <div key={item._id} className='topiclist__pastylla__section'>
                  <Preview {...item} />
                </div>
              );
            })}
          </div>
        )}
      </Sidebar>
    );
  }

  render() {
    const { loading } = this.state;
    const { topicList, location, pastylla } = this.props;
    const showFaq = !loading && !pastylla.length;

    if (location.pathname === '/topic') return <Redirect to='/' />;

    return (
      <Layout template={showFaq ? 'topic-faq': 'topic-pastylla'}>
        {this.renderActionbar()}
        <div className='topiclist'>
          <div className='topiclist__section topiclist__section-pastylla'>
            {!showFaq && (
              <div className='topiclist__pastylla'>
                <div className="topiclist__pastylla__section topiclist__pastylla__section-header">
                  <div className="topiclist__pastylla__header">
                    <Title level={5} template='black'>Мои презентации</Title>
                  </div>
                </div>
                <Slider fade={false} progressbar={false} info={false}>
                  {pastylla.map(item => {
                    return (
                      <div key={item._id} className='topiclist__pastylla__item'>
                        <Preview {...item} />
                      </div>
                    );
                  })}
                </Slider>
              </div>
            )}
          </div>
          <div className='topiclist__section topiclist__section-title'>
            <Title level={3} template='purple'>Я хочу,</Title>
          </div>
          {topicList && topicList.map(item => {
            return (
              <div key={item._id} className='topiclist__section topiclist__section-item'>
                <Topic {...item} />
              </div>
            );
          })}
        </div>
        {this.renderSidebar()}
      </Layout>
    );
  }
}

const mapStateToProps = ({ pastylla }) => ({ pastylla: pastylla.items });
const mapDispatchToProps = dispatch => bindActionCreators({ fetchPastyllaList }, dispatch);
 
export default connect(mapStateToProps, mapDispatchToProps)(withTopicList(TopicList));