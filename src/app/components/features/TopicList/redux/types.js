import { PROJECT_NAME } from '@bootstrap/constants';

export const COMPONENT_TOPICLIST_FETCH = `@@${PROJECT_NAME}/component/topiclist/FETCH`;
