import { COMPONENT_TOPICLIST_FETCH } from './types';

const initialState = {
  items: []
}

export default (state = initialState, action) => {
  switch (action.type) {
    case COMPONENT_TOPICLIST_FETCH:
      return Object.assign({}, state, { items: action.items });
    default:
      return state;
  }
};
