import 'isomorphic-fetch';

import { API_URL } from '@bootstrap/constants';
import { COMPONENT_TOPICLIST_FETCH } from './types';

import { fetchData } from '@bootstrap/redux/actions';

const callback = (data, dispatch) => {
  return data.json().then(_json => {
    // TODO: Set proper error handling
    if(_json.error) return console.log(_json.error);

    return dispatch({ type: COMPONENT_TOPICLIST_FETCH, items: _json });
  });
}

export const fetchTopicList = (request) => fetchData(
  `${API_URL}/topic`, 
  callback, 
  'GET', 
  null, 
  request
);
