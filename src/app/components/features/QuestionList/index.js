import './style.css';
import './widescreen.css';
import './tablet.css';
import './mobile.css';

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import generateArchive from '@helpers/generateArchive';
import { createPastylla, updatePastylla } from '@features/Pastylla/redux/actions';
import { fetchDesign } from '@features/Design/redux/actions';

import Layout from '@containers/Layout';
import Sidebar from '@containers/Sidebar';
import Actionbar from '@containers/Actionbar';

import Title from '@ui/Title';
import Text from '@ui/Text';
import Button from '@ui/Button';
import Drop from '@ui/Drop';
import Counter from '@ui/Counter';
import Textarea from '@ui/Textarea';
import Progress from '@ui/Progress';
import OrderedList from '@ui/OrderedList';

class QuestionList extends Component {
  static propTypes = {
    id: PropTypes.string,
    topic: PropTypes.object,
    createPastylla: PropTypes.func,
    stream: PropTypes.object,
    pastylla: PropTypes.object
  };

  constructor(props) {
    super();

    const { history } = props;
    const { location } = history;
    const { state } = location;  

    this.state = { step: state && state.step ? state.step : 0 };
  }

  componentDidMount() {
    this.field.focus();
  }

  createPastylla = () => {
    const { topic, createPastylla, fetchDesign } = this.props;
    const reply = this.field.value;
    
    topic.design.reverse();

    const defaultTemplateId = topic.design.length ? topic.design[0] : null;

    if (defaultTemplateId) {
      return fetchDesign(defaultTemplateId).then(template => {
        // Create Pastylla. Flow from TopicList.
        const content = [
          { id: this.question.name, value: reply }
        ];
        const _pastylla = { topic, content };

        return generateArchive(template, _pastylla).then(_presentation => {
          const _form = new FormData();
          _form.append('file', _presentation, `${Date.now()}.pastylla`);

          return createPastylla(_form);
        });
      });
    }
  }

  updatePastylla = () => {
    const { id, pastylla, updatePastylla, stream } = this.props;
    const reply = this.field.value;

    // Update Pastylla. Flow from /:pastyllaId/questions
    const { content } = pastylla;
    const answer = content.find(item => item.id === this.question.name);

    if (answer) content.splice(content.indexOf(answer), 1);
    content.push({ id: this.question.name, value: reply });

    const _pastylla = Object.assign({}, pastylla, { content });

    return generateArchive(stream, _pastylla).then(_presentation => {
      const _form = new FormData();
      _form.append('file', _presentation, `${Date.now()}.pastylla`);

      return updatePastylla(_form, id);
    });
  }

  handleSwitchQuestion = (direction) => {
    const { id, topic, pastylla, history, createPastylla, updatePastylla } = this.props;
    const { step } = this.state;
    const next = direction === 'next' ? step + 1 : step - 1;
    const reply = this.field.value;
    
    if (!reply) {
      if (direction === 'prev') {
        history.replace(`/${id}/questions`, { step: next < 0 ? 0 : next });

        this.setState({ step: next < 0 ? 0 : next });
        this.field.focus();
      } else {
        // TODO: Error flow if reply is empty
        return;
      }
    }

    if (step === 0 && !id) {
      return this.createPastylla().then(data => {
        history.replace(`/${data._id}/questions`, { step: next });
      });
    } else {
      return this.updatePastylla().then(data => {
        this.setState({ step: next < 0 ? 0 : next });
        this.field.focus();

        history.replace(`/${id}/questions`, { step: next < 0 ? 0 : next });
      });
    }
  }

  handleSelectDesign = () => {
    const { topic, id, history } = this.props;
    const { questions } = topic;

    return this.updatePastylla().then(data => {
      this.setState({ step: questions.length - 1 });

      history.replace(`/${id}/design`);
    });
  }

  renderActionbar() {
    const params = {
      items: [
        { _id: '1somes', icon: 'burger', template: "transparent" },
        { _id: '2somes', icon: 'play' },
        { _id: '3somes', icon: 'share' }
      ]
    };

    return <Actionbar {...params} />;
  }

  renderSidebar() {
    const { recommendation } = this.question;
    const params = {
      template: 'questionlist',
      previousPageLink: {
        title: 'К выбору темы',
        href: '/'
      }
    };

    return (
      <Sidebar {...params}>
        {recommendation && (<Title level={3}>Рекомендации</Title>)}
        {recommendation && (
          <div className='questionlist__recommendation'>
            <div className='questionlist__recommendation__section questionlist__recommendation__section-main'>
              <div className='questionlist__main'>
                {recommendation.main.map((section, i) => {
                  return (
                    <div key={`${this.question._id}_${i}`} className='questionlist__main__section'>
                      <div className='questionlist__text'>
                        <Text size={14}>
                          {section}
                        </Text>
                      </div>
                    </div>
                  );
                })}
              </div>
            </div>
            <div className='questionlist__recommendation__section questionlist__recommendation__section-additional'>
              <div className='questionlist__additional'>
                <div className='questionlist__additional__section questionlist__additional__section-title'>
                  <Title level={5}>Не забудьте</Title>
                </div>
                <div className='questionlist__additional__section questionlist__additional__section-orderedlist'>
                  <OrderedList items={recommendation.additional} />
                </div>
              </div> 
            </div>
          </div>       
        )}
      </Sidebar>
    );
  }

  renderProgress() {
    const { step } = this.state;
    const { topic } = this.props;
    const progress = (step / (topic.questions.length - 1)) * 100;

    return <Progress progress={progress} />;
  }

  renderCounter() {
    const { step } = this.state;
    const { topic } = this.props;

    return <Counter current={step + 1} length={topic.questions.length} />;
  }

  renderTitle() {
    return <Title level={2}>{this.question.title}</Title>;
  }

  renderDescription() {
    if (this.question.description) return <Text size={25}>{this.question.description}</Text>;
  }

  renderField() {
    const { pastylla } = this.props;
    const params = {
      onEnter: this.handleNext,
      r: (e) => {
        this[this.question._id] = e;
        this.field = e;
      }
    };

    if (pastylla) {
      const { content } = pastylla;
      const answer = content.find(item => item.id === this.question.name);

      if (answer) params.defaultValue = answer.value;
    }

    return <Textarea key={this.question._id} {...params}></Textarea>;
  }

  renderSettings() {
    const { settings } = this.question;

    return (
      <div className='questionlist__settings'>
        <div className='questionlist__settings__section questionlist__settings__section-symbols'>
          <div className='questionlist__symbols'>
            <Text size={18} bold template='grey'>{settings.symbols} символов осталось</Text>
          </div>
        </div>
      </div>
    );
  }

  renderActions() {
    const { step } = this.state;
    const { topic } = this.props;
    const { questions } = topic;
    const actions = [
      { 
        id: 'prev',
        element: <Drop onClick={() => this.handleSwitchQuestion('prev')} icon={ {name:'angle', direction:'left'}} />,
        isDisplay: step > 0
      },
      {
        id: 'next',
        element: (
          <div className='questionlist__actions__next'>
            <Drop onClick={() => this.handleSwitchQuestion('next')} icon={ {name:'angle', direction:'right'}} />
          </div>
        ),
        isDisplay: step < questions.length - 1
      },
      {
        id: 'toDesign',
        element: <Button suffux='arrow' onClick={this.handleSelectDesign}>Выбрать стилистику</Button>,
        isDisplay: step === questions.length - 1
      }
    ]; 

    return (
      <div className='questionlist__actions'>
        {actions.map(action => {
          if (action.isDisplay) {
            return (
              <div key={action._id} className={`questionlist__actions__section questionlist__actions__section-${action.id}`}>
                {action.element}
              </div>
            );
          }
        })}
      </div>
    );
  }

  render() {
    const { step } = this.state;
    const { topic } = this.props;

    if (!topic) return <Redirect to='/error/404' />

    const { questions } = topic;

    this.question = questions.find((item, i) => i === step);
    
    return (
      <Layout>
        {this.renderActionbar()}
        <div className='questionlist'>
          <div className='questionlist__section questionlist__section-progressbar'>
            {this.renderProgress()}
          </div>
          <div className='questionlist__section questionlist__section-counter'>
            {this.renderCounter()}
          </div>
          <div className='questionlist__section questionlist__section-title'>
            {this.renderTitle()}
          </div>
          <div className='questionlist__section questionlist__section-description'>
            {this.renderDescription()}
          </div>
          <div className='questionlist__section questionlist__section-field'>
            {this.renderField()}
          </div>
          <div className='questionlist__section questionlist__section-settings'>
            {this.renderSettings()}
          </div>
          <div className='questionlist__section questionlist__section-actions'>
            {this.renderActions()}
          </div>
        </div>
        {this.renderSidebar()}
      </Layout>
    );
  }
}

export default connect(null, dispatch => bindActionCreators({ createPastylla, updatePastylla, fetchDesign }, dispatch))(QuestionList);