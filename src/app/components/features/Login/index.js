import "./style.css";
import "./mobile.css";
import "./tablet.css";
import "./widescreen.css";

import React, { Component } from "react";
import PropTypes from "prop-types";
import Redirect from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { login } from '@features/Auth/redux/actions';

import AuthForm from "@containers/AuthForm";

class Login extends Component {
  static propTypes = {
    login: PropTypes.func,
    location: PropTypes.object
  };
  render() {
    const { location } = this.props;
  
    if (location.pathname === '/auth') return <Redirect to='/auth/login' />;

    const params = {
      fields: [
        {
          id: "email",
          type: "text",
          label: "E-mail",
          message: "Введите электронную почту",
          error: { message: "try something else", show: false }
        },
        {
          id: "password",
          type: "password",
          label: "Пароль",
          message: "Введите ваш пароль",
          error: { message: "try something else", show: false }
        }
      ],
      links: [
        { name:"login", href:"/auth/login" },
        { name:"restore", href:"/auth/restore" },
        { name:"signup", href:"/auth/signup" }
      ],
      button: 'Войти',
      r: (id, e) => this[id] = e,
      onClick: () => {
        const email = this.email.value;
        const password = this.password.value;

        return this.props.login({ email, password });
      }
    };

    return <AuthForm {...params} />;
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({ login }, dispatch);

export default connect(null, mapDispatchToProps)(Login);
