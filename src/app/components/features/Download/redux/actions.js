import 'isomorphic-fetch';

import { API_URL } from '@bootstrap/constants';

import { fetchData } from '@bootstrap/redux/actions';

export const downloadJpg = (data) => fetchData(
  `/export/v1/jpg`, 
  res => res.json(), 
  'POST', 
  data,
  null,
  true
);

export const downloadPdf = (data) => fetchData(
  `/export/v1/pdf`, 
  res => res.blob(), 
  'POST', 
  data,
  null,
  true
)