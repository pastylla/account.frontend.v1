import "./style.css";
import "./widescreen.css";
import "./tablet.css";
import "./mobile.css";

import React, { Component } from "react";
import PropTypes from "prop-types";

import Layout from "@containers/Layout";
import Sidebar from "@containers/Sidebar";
import Actionbar from "@containers/Actionbar";
import Recommendations from "@containers/Recommendations";

import Title from "@ui/Title";
import Text from "@ui/Text";
import Radio from "@ui/Radio";
import Button from "@ui/Button";
import Icon from "@ui/Icon";
import Drop from "../../ui/Drop";

class Download extends Component {
  static propTypes = {};

  constructor() {
    super();
    this.state = {
      conditions: [
        {
          id: "0",
          title: "Скачать для",
          radio: [{ title: "Печати" }, { title: "Отправке по e-mail" }]
        },
        {
          id: "1",
          title: "Формат",
          radio: [{ title: ".PDF" }, { title: ".PPTX" }]
        },
        {
          id: "2",
          title: "Соотношение сторон",
          radio: [{ title: "4:3" }, { title: "16:9" }]
        }
      ],
      choices: {}
    };
  }

  handleChangesChoice = (id, item) => {
    const { choices } = this.state;

    let newChoices = choices;
    newChoices[id] = item.title;

    this.setState({ choices: newChoices });
  };

  handleClick = () => {
    const { choices } = this.state;

    console.log(choices);
  };

  renderActionbar() {
    const params = {
      template: "white"
    };

    return <Actionbar {...params} />;
  }

  renderSidebar() {
    const recommendations = [
      {
        title: "Формат",
        description:
          "Если вы не можете гаран­ти­ро­вать эффект, честно при­знайте, что вы лишь пред­по­ла­га­ете его или опи­ра­е­тесь на преды­ду­щий опыт."
      },
      {
        title: "Формат",
        description:
          "Если вы не можете гаран­ти­ро­вать эффект, честно при­знайте, что вы лишь пред­по­ла­га­ете его или опи­ра­е­тесь на преды­ду­щий опыт."
      },
      {
        title: "Формат",
        description:
          "Если вы не можете гаран­ти­ро­вать эффект, честно при­знайте, что вы лишь пред­по­ла­га­ете его или опи­ра­е­тесь на преды­ду­щий опыт."
      }
    ];
    return (
      <Sidebar template="download">
        <Title level={3} template="white">
          Рекомендации
        </Title>
        <Recommendations items={recommendations} />
        <div className="download__advice">
          <div className="download__advice__section download__advice__section-text">
            <Text size={16} template="white">
            Не стыдно ска­зать, что могут быть труд­но­сти. Так к вам больше доверия
            </Text>
          </div>
          <div className="download__advice__section download__advice__section-icon">
            <Drop icon="question"/>
          </div>
        </div>
      </Sidebar>
    );
  }

  renderConditions() {
    const { conditions } = this.state;
    return (
      <div className="download__conditions">
        <div className="download__conditions__section download__conditions__section-format">
          {!!conditions &&
            conditions.map((condition, index) => {
              return (
                <div
                  key={`${condition.title}_${index}`}
                  className="download__conditions__item"
                >
                  <div className="download__conditions__item__section download__conditions__item__section-title">
                    <Title level={4} template="white">
                      {condition.title}
                    </Title>
                  </div>
                  <div className="download__conditions__item__section download__conditions__item__section-radio">
                    <Radio
                      onChanges={this.handleChangesChoice}
                      id={condition.id}
                      items={condition.radio}
                    />
                  </div>
                </div>
              );
            })}
        </div>
      </div>
    );
  }

  render() {
    const { choices } = this.state;
    return (
      <Layout template="black">
        {this.renderActionbar()}
        <div className="download">
          <div className="download__section download__section-title">
            <Title level={2} template="primary">
              Последние
            </Title>
            <Title level={2} template="white">
              штрихи
            </Title>
          </div>
          <div className="download__section download__section-subtitle">
            <Title level={3} template="white">
              Выберите удобный для Вас формат
            </Title>
          </div>
          <div className="download__section download__section-conditions">
            {this.renderConditions()}
          </div>
          <div className="download__section download__section-button">
            <button onClick={this.handleClick} className="download__button">
              <Title level={4} template="white">
                Скачать презентацию
              </Title>
              <br />
              <Text size={18} template="white">
                {Object.values(choices).join(",")}
              </Text>
            </button>
          </div>
        </div>
        {this.renderSidebar()}
      </Layout>
    );
  }
}

export default Download;
