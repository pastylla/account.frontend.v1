import { COMPONENT_AUTH_IDENTIFY } from './types';

export default (state = {}, action) => {
  switch (action.type) {
    case COMPONENT_AUTH_IDENTIFY:
      return Object.assign({}, state, { user: action.user });
    default:
      return state;
  }
};
