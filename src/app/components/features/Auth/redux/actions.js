import 'isomorphic-fetch';

import { AUTH_URL } from '@bootstrap/constants';
import { COMPONENT_AUTH_IDENTIFY } from './types';

import { fetchData } from '@bootstrap/redux/actions';

const authentificationCallback = (data, dispatch) => {
  // TODO: Set proper error handling
  if(data.error) return console.log(data.error);

  if (data instanceof Object) {
    const _storage = window.localStorage;
    _storage.setItem('token', data.token);

    return dispatch({ type: COMPONENT_AUTH_IDENTIFY, user: data.user });
  }
  else return dispatch({ type: COMPONENT_AUTH_IDENTIFY, user: null });
}

const callback = (data, dispatch) => {
  // TODO: Set proper error handling
  if(data.error) return console.log(data.error);

  return dispatch({ type: COMPONENT_AUTH_IDENTIFY, user: data });
}

const logoutCallback = (data, dispatch) => {
  callback(data, dispatch);

  window.location.reload();
}

export const fetchUser = (request) => fetchData(`${AUTH_URL}/`, callback, 'GET', null, request);
export const updateUser = (data) => fetchData(`${AUTH_URL}/`, callback, 'POST', JSON.stringify(data));


export const login = (data) => fetchData(`${AUTH_URL}/login`, authentificationCallback, 'POST', JSON.stringify(data));
export const signup = (data) => fetchData(`${AUTH_URL}/signup`, authentificationCallback, 'POST', JSON.stringify(data));
export const logout = (request) => fetchData(`${AUTH_URL}/logout`, logoutCallback, 'GET', null, request);

export const restore = data => console.log(data);