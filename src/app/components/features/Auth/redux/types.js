import { PROJECT_NAME } from '@bootstrap/constants';

export const COMPONENT_AUTH_IDENTIFY = `@@${PROJECT_NAME}/component/auth/IDENTIFY`;
