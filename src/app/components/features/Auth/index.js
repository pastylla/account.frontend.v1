import "./style.css";
import "./widescreen.css";
import "./tablet.css";
import "./mobile.css";

import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { renderRoutes } from "react-router-config";
import { Redirect } from "react-router-dom";

import Slider from "@ui/Slider";
import Title from "@ui/Title";
import Deck from "@ui/Deck";
import Button from "@ui/Button";
import Tabs from "@ui/Tabs";
import Logo from "@ui/Logo";

class Auth extends Component {
  static propTypes = {
    user: PropTypes.object,
    route: PropTypes.object
  };

  constructor() {
    super();
    this.state = {
      onboardingHidden: false
    };
  }

  onboardingHandler = () => {
    const { onboardingHidden } = this.state;
    this.setState({ onboardingHidden: !onboardingHidden });
  };

  renderAction() {
    const { route } = this.props;

    const tabs = [
      { name: "Регистрация", to: "signup" },
      { name: "Вход", to: "login" }
    ];

    tabs.map(tab => {
      if (typeof window !== "undefined") {
        let location = window.location.pathname;
        tab.active = location.slice(location.lastIndexOf("/") + 1) === tab.to;
      }
    });
    return (
      <div className="auth__action">
        <div className="auth__action__section auth__action__section-brand">
          <Logo />
        </div>
        <div className="auth__action__section auth__action__section-title">
          <Title level={1}>Pastila</Title>
        </div>
        <div className="auth__action__section auth__action__section-subtitle">
          <Title level={5}>Сервис создания презентаций</Title>
        </div>
        <div className="auth__action__section auth__action__section-tabs">
          <Tabs tabs={tabs} />
        </div>
        <div className="auth__action__section auth__action__section-content">
          {renderRoutes(route.routes)}
        </div>
      </div>
    );
  }

  renderOnboarding() {
    return (
      <div className="auth__onboarding">
        <div className="auth__onboarding__section auth__onboarding__section-brand">
          <Logo template="white" />
        </div>
        <div className="auth__onboarding__section auth__onboarding__section-subtitle">
          <Title level={5} template="white">
            FAQ
          </Title>
        </div>
        <div className="auth__onboarding__section auth__onboarding__section-title">
          <Title level={3} template="white">
            Любую презентацию можно создать в три простых шага
          </Title>
        </div>
        <div className="auth__onboarding__section auth__onboarding__section-slider">
          <Slider>
            <Deck />
            <Deck />
            <Deck />
          </Slider>
        </div>
        <div className="auth__onboarding__section auth__onboarding__section-button">
          <Button size="long" onClick={this.onboardingHandler}>
            Понятно
          </Button>
        </div>
      </div>
    );
  }

  render() {
    const { onboardingHidden } = this.state;
    const { location } = this.props;

    if (location.pathname === "/auth") return <Redirect to="/auth/login" />;

    return (
      <div className="auth">
        <div
          className={`auth__section auth__section-action ${
            !onboardingHidden ? "auth__section--hidden" : ""
          }`}
        >
          {this.renderAction()}
        </div>
        <div
          className={`auth__section auth__section-onboarding ${
            onboardingHidden ? "auth__section--hidden-no-transition" : ""
          }`}
        >
          {this.renderOnboarding()}
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ auth }) => ({ user: auth.user });

export default connect(mapStateToProps)(Auth);
