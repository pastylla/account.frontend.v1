import "./style.css";
import "./mobile.css";
import "./tablet.css";
import "./widescreen.css";

import React, { Component } from "react";
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import PropTypes from "prop-types";

import { signup } from '@features/Auth/redux/actions';

import AuthForm from "@containers/AuthForm";

class Signup extends Component {
  static propTypes = {
    signup: PropTypes.func
  };

  render() {
    const params = {
      fields: [
        {
          id: "name",
          type: "text",
          label: "E-mail",
          message: "Введите вашу почту",
          r: e => this.email = e,
          error: { message: "try something else", show: false }
        },
        {
          id: "mail",
          type: "text",
          label: "Пароль",
          message: "Введите ваш пароль",
          r: e => this.password = e,
          error: { message: "try something else", show: false }
        }
      ],
      links: [
        { name: "login", href: "/auth/login" },
        { name: "restore", href: "/auth/restore" },
        { name: "signup", href: "/auth/signup" }
      ],
      button: 'Зарегистрироваться',
      r: (id, e) => this[id] = e,
      onClick: this.props.signup
    };

    return<AuthForm {...params} />;
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({ signup }, dispatch);

export default connect(null, mapDispatchToProps)(Signup);
