import './style.css';
import './widescreen.css';
import './tablet.css';
import './mobile.css';

import React from 'react';

import Button from '@ui/Button';

export default ({ preview, templates, onTemplateSelect }) => {
  return (
    <div className='slide'>
      <img className='slide__image' src={preview} />
      <div className='slide__section slide__section-actionbar'>
        <nav className='slide__actionbar'>
          <div className="slide__actionbar__section">
            <Button size="minimal" template='transparent' prefix='comment'>Добавить комментарий</Button>
          </div>
          <div className="slide__actionbar__section">
            <Button size="minimal" template='transparent' prefix='edit'>Редактировать</Button>
          </div>
          <div className="slide__actionbar__section">
            <Button size="minimal" template='transparent' prefix='picture'>Изменить картинку</Button>
          </div>
        </nav>
      </div>
      <div className='slide__section slide__section-template'>
        <div className="slide__template">
          {templates.map(_t => {
            return (
              <div key={_t._id} className='slide__template__section'>
                <div className="slide__preview" onClick={onTemplateSelect} />
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
}