import './style.css';
import './widescreen.css';
import './tablet.css';
import './mobile.css';

import React from 'react';

import Title from '@ui/Title';
import Text from '@ui/Text';
import Tag from '@ui/Tag';

export default ({ items }) => {
  return (
    <div className='roadmap'>
      {items.map((item, i) => {
        return (
          <div key={i} className='roadmap__section'>
            <div className='roadmap__item'>
              <div className='roadmap__item__section roadmap__item__section-header'>
                <div className='roadmap__header'>
                  <div className='roadmap__header__section roadmap__header__section-title'>
                    <div className='roadmap__title'>
                      <Title level={5}>{`0${i + 1}`}. {item.title}</Title>
                    </div>
                  </div>
                  {!!item.active && (
                    <div className='roadmap__header__section roadmap__header__section-tag'>
                      <Tag size='rectangle' template='purple'>Вы на этом шаге</Tag> 
                    </div>
                  )}
                </div>
              </div>
              <div className='roadmap__item__section roadmap__item__section-description'>
                <Text size={15} template='grey'>{item.description}</Text>
              </div>
            </div>
          </div>
        );
      })}
    </div>
  );
}