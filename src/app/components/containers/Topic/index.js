import './style.css';
import './widescreen.css';
import './tablet.css';
import './mobile.css';

import React from 'react';
import { Link } from 'react-router-dom';

import Tag from '@ui/Tag';
import Text from '@ui/Text';

export default ({ _id, title, description, tags }) => {
  return (
    <div className='topic'>
      <div className='topic__section topic__section-title'>
        <Link to={`/topic/${_id}`} className='topic__title'>{title}</Link>
      </div>
      <div className='topic__section topic__section-description'>
        <Text size={18} template='gray'>{description}</Text>
      </div>
      <div className='topic__section topic__section-tags'>
        <div className='topic__tags'>
        {tags.map(item => {
          return (
            <div key={item._id} className='topic__tags__section'>
              <Tag {...item}>{item.title}</Tag>
            </div>
          );
        })}
        </div>
      </div>
    </div>
  );
}