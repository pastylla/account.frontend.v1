import "./style.css";
import "./mobile.css";
import "./tablet.css";
import "./widescreen.css";

import React from "react";

import Input from "@ui/Input";
import Button from "@ui/Button";
import Text from "@ui/Text";

export default ({ fields, button, onClick, links, r }) => {
  return (
    <div className="auth-form">
      <div className="auth-form__section auth-form__section-inputs">
        <div className="auth-form__inputs">
          {fields.map(item => {
            const params = {
              error: item.error,
              message: item.message,
              label: item.label,
              template: 'block',
              r: (e) => r(item.id, e),
              value: item.value,
              type: item.type ? item.type : 'text'
            };
    
            return (
              <div key={item.id} className={`auth-form__inputs__section auth-form__inputs__section-${item.id}`}>
                <Input {...params} />
              </div>
            );
          })}
        </div>
      </div>
      <div className="auth-form__section auth-form__section-registration">
        <Button onClick={onClick} size="long">{button}</Button>
      </div>
      <div className="auth-form__section auth-form__section-login">
        <div className="auth-form__login">
          <div className="auth-form__login__section auth-form__login__section-label">
            <Text size={14} template="grey">Или войти с помощью</Text>
          </div> 
          {links.map(item => {
            return(
              <div key={item.name} className="auth-form__login__section auth-form__login__section-button">
                <Button type="link" template="white" to={item.href}>{item.name}</Button>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
}