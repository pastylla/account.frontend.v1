import "./style.css";
import "./widescreen.css";
import "./tablet.css";
import "./mobile.css";

import React from "react";

import Logo from "@ui/Logo";
import Drop from "@ui/Drop";

export default ({
  displayLogo = true,
  displayClose = false,
  displayCounter = false,
  template = "default",
  onClose,
  items
}) => {
  return (
    <div className={`actionbar actionbar--${template}`}>
      {displayLogo && (
        <div className="actionbar__section actionbar__section-logo">
          <Logo template={template} />
        </div>
      )}
      {displayClose && (
        <div className="actionbar__section actionbar__section-close">
          <div className="actionbar__close">
            <Drop onClick={onClose} />
          </div>
        </div>
      )}
      <div className="actionbar__section actionbar__section-items">
        <div className="actionbar__items">
          {!!items &&
            items.map(item => {
              const params = { icon: item.icon, template: item.template };

              ["onClick", "href", "to"].map(key =>
                item[key] ? (params[key] = item[key]) : null
              );

              return (
                <div key={item._id} className="actionbar__items__section">
                  <Drop {...params} />
                </div>
              );
            })}
        </div>
      </div>
      {displayCounter && (
        <div className="actionbar__section actionbar__section-counter">
          {this.renderCounter()}
        </div>
      )}
    </div>
  );
};
