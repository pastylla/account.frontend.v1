import "./style.css";
import "./widescreen.css";
import "./tablet.css";
import "./mobile.css";

import React from "react";

import Icon from "@ui/Icon";
import Title from "@ui/Title";

export default ({
  template = "default",
  children,
  open = false,
  action,
  title,
  onClick
}) => {
  return (
    <div
      className={`drawer drawer-${template} drawer--${
        open ? "open" : "closed"
      }`}
    >
      <div className="drawer__section drawer__section-header">
        <div className="drawer__header">
          <div
            onClick={onClick ? onClick : () => {}}
            className="drawer__header__section drawer__header__section-icon"
          >
            <Icon name="angle" template="white" direction="left" />
          </div>
          <div className="drawer__header__section drawer__header__section-title">
            <Title level={5} template="white">
              {title}
            </Title>
          </div>
          {action && (
            <div
              onClick={action.func ? action.func : () => {}}
              className="drawer__header__section drawer__header__section-action"
            >
              <Title level={5} template="primary">
                {action.title}
              </Title>
            </div>
          )}
        </div>
      </div>
      <div className="drawer__section drawer__section-content">
        {children.map((section, i) => {
          return (
            <div
              key={i}
              className={`drawer__content__section drawer__content__section-${template}`}
            >
              {section}
            </div>
          );
        })}
      </div>
    </div>
  );
};
