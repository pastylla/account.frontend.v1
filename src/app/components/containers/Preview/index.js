import './style.css';
import './widescreen.css';
import './tablet.css';
import './mobile.css';

import React from 'react';
import { Link } from 'react-router-dom';

import Title from '@ui/Title';
import Text from '@ui/Text';
import Progress from '@ui/Progress';

export default ({ _id, title }) => {
  return (
    <Link to={`/${_id}`} className='preview'>
      <div className='preview__section preview__section-title'>
        <Title level={5} template='black'>{title}</Title>
      </div>
      <div className='preview__section preview__section-progress'>
        <Text size={14} template='grey'>Завершено на 20%</Text>
      </div>
      <div className='preview__section preview__section-progressbar'>
        <Progress template='pastylla' progress={20} />
      </div>
    </Link>
  );
}