import './style.css';
import './widescreen.css';
import './tablet.css';
import './mobile.css';

import React from 'react';

import Profile from '@features/Profile';

import Link from '@ui/Link';
import Icon from '@ui/Icon';

export default ({ template = 'default', children, previousPageLink }) => {
  return (
    <div className={`sidebar sidebar--${template}`}>
      <div className='sidebar__section sidebar__section-header'>
        <div className='sidebar__header'>
          <div className='sidebar__header__section sidebar__header__section-link'>
            {previousPageLink && (
              <Link  before={<Icon name='arrow' />} to={previousPageLink.href} size={16}>
                {previousPageLink.title}
              </Link>
            )}
          </div>
        </div>
      </div>
      <div className='sidebar__section sidebar__section-content'>
        <div className={`sidebar__content sidebar__content-${template}`}>
          {children && children.map((section, i) => {
            return (
              <div key={i} className={`sidebar__content__section sidebar__content__section-${template}`}>
                {section}
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
}