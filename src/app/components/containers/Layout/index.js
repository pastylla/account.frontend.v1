import './style.css';
import './widescreen.css';
import './tablet.css';
import './mobile.css';

import React from 'react';
import Profile from '@features/Profile';

export default ({ children, template='default' }) => {
  return (
    <div className={`layout layout--${template}`}>
      {children.map((section, i) => {
        let purpose;

        switch (i) {
          default:
          case 0:
            purpose = 'actionbar';
            break;
          case 1:
            purpose = 'content';
            break;
          case 2:
            purpose = 'sidebar';
            break;
        }

        return (
          <div key={i} className={`layout__section layout__section-${purpose}`}>
            {section}
          </div>
        );
      })}
       <div className='layout__section layout__section-profile'>
          <Profile />
        </div>
    </div>
  );
}