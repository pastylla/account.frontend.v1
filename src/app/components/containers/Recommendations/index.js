import "./style.css";
import "./widescreen.css";
import "./tablet.css";
import "./mobile.css";

import React from "react";

import Title from "@ui/Title";
import Text from "@ui/Text";

export default ({ items }) => {
  return (
    <div className="recommendations">
      {!!items &&
        items.map((recommendation, index) => {
          return (
            <div
              key={`${recommendation.title}_${index}`}
              className="recommendations__section recommendations__section-item"
            >
              <div className="recommendations__item">
                <div className="recommendations__item__section recommendations__item__section-title">
                  <Title level={4} template="white">
                    {recommendation.title}
                  </Title>
                </div>
                <div className="recommendations__item__section recommendations__item__section-description">
                  <Text size={14} template="white">
                    {recommendation.description}
                  </Text>
                </div>
              </div>
            </div>
          );
        })}
    </div>
  );
};
