import "./style.css";
import "./widescreen.css";
import "./tablet.css";
import "./mobile.css";

import React, { Component } from "react";

import Accordion from "@ui/Accordion";
import Title from "@ui/Title";

class DropDownList extends Component {
  constructor() {
    super();
    this.state = {
      open: false,
      current: 0
    };
  }

  handleToggleOpen = () => {
    const { open } = this.state;
    this.setState({ open: !open });
  };

  handleChange = index => {
    this.setState({ current: index });
  };

  render() {
    const { open, current } = this.state;
    const { template = "default", items, item, active } = this.props;
    return (
      <div className="drop-down-list">
        <Accordion open={open}>
          <div
            onClick={this.handleToggleOpen}
            className="drop-down-list__section drop-down-list__section-title"
          >
            <div className="drop-down-list__title">{item}</div>
          </div>
          <div className="drop-down-list__section drop-down-list__section-list">
            <ul className={`drop-down-list__list`}>
              {items.map((item, i) => {
                let mod =
                  current === i ? "current" : current > i ? "previous" : "next";
                return (
                  <li
                    key={`${template}_${i}_${i + 1}`}
                    onClick={() => this.handleChange(i)}
                    className={`drop-down-list__list__section  drop-down-list__list__section--${mod}`}
                  >
                    <div
                      className={`drop-down-list__list__item drop-down-list__list__item--${mod}`}
                    >
                      {item}
                    </div>
                  </li>
                );
              })}
            </ul>
          </div>
        </Accordion>
      </div>
    );
  }
}

export default DropDownList;
