import './style.css';
import './widescreen.css';
import './tablet.css';
import './mobile.css';

import React from 'react';
import { Link } from 'react-router-dom';

export default ({ template }) => {
  const params = {
    className: `logo logo--${template ? template : 'default'}`
  };

  return <Link {...params} to='/'>pastila</Link>;
}