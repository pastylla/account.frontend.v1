import "./style.css";
import "./mobile.css";
import "./tablet.css";
import "./widescreen.css";

import React from "react";

export default ({ level, template = "default", children }) => {
  const params = { className: `title title--${template}` };

  switch (level) {
    case 6:
      return <h6 {...params}>{children}</h6>;
    case 5:
      return <h5 {...params}>{children}</h5>;
    case 4:
      return <h4 {...params}>{children}</h4>;
    case 3:
      return <h3 {...params}>{children}</h3>;
    case 2:
      return <h2 {...params}>{children}</h2>;
    case 1:
    default:
      return <h1 {...params}>{children}</h1>;
  }
};
