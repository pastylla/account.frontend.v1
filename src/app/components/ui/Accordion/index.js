import "./style.css";
import "./widescreen.css";
import "./tablet.css";
import "./mobile.css";

import React, { Component } from "react";

class Accordion extends Component {
  render() {
    const { open, children } = this.props;
    const firstChild = children.slice(0, 1);
    const nextChildren = children.slice(1);

    return (
      <div className="accordion">
        <div
          onClick={this.handleToggle}
          className="accordion__section accordion__section-item"
        >
          {firstChild}
        </div>
        <div
          className={`accordion__section accordion__section-content accordion__section--${
            open ? "open" : "closed"
          }`}
        >
          {nextChildren}
        </div>
      </div>
    );
  }
}

export default Accordion;
