import "./style.css";
import "./mobile.css";
import "./tablet.css";
import "./widescreen.css";

import React from "react";
import { Link as RouterLink } from "react-router-dom";

export default ({
  size = 16,
  template = "default",
  to,
  href,
  anchor,
  children,
  onClick,
  before,
  after
}) => {
  const params = {
    className: `link link-${size} link--${template}`,
    onClick
  };

  const content = [];
  
  if (before) content.push(<span key='before' className='link__section link__section-before'>{before}</span>);
  content.push(<span key='children' className='link__section link__section-children'>{children}</span>);
  if (after) content.push(<span key='after' className='link__section link__section-after'>{after}</span>); 


  switch (true) {
    case !!to:
      return (
        <RouterLink to={to} {...params}>
          {content}
        </RouterLink>
      );
    case !!href:
      return (
        <a href={href} target="_blank" {...params}>
          {content}
        </a>
      );
    case !!anchor:
      return (
        <a href={anchor} {...params}>
          {content}
        </a>
      );
    default:
      return (
        <a {...params}>
          {content}
        </a>
      );
  }
};
