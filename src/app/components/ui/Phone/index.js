import "./style.css";
import './mobile.css';
import './tablet.css';
import './widescreen.css';

import React from 'react';

export default ({ children = '79256403633' } ) => {
  return (
    <a className='phone' href={`tel:${children}`}>
      {children.replace(/(\d{1})(\d{3})(\d{3})(\d{2})(\d{2})/, '+$1 /$2/ $3-$4-$5')}
    </a>
  );
}
