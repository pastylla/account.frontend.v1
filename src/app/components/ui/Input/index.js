import './style.css';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import InputMask from 'react-input-mask';

class Input extends Component {
  static propTypes = {
    type: PropTypes.string,
    label: PropTypes.string,
    r: PropTypes.func,
    error: PropTypes.object,
    defaultValue: PropTypes.node,
    modifiers: PropTypes.array,
    params: PropTypes.object
  };

  handleFocus = () => {
    this.placeholder.classList.add('input__section-label');
    this.placeholder.classList.remove('input__section-placeholder');
    this.label.classList.add('input__label--focus');
  }

  handleBlur = () => {
    if (!this.field.value || this.field.value === ' ') {
      this.placeholder.classList.remove('input__section-label');
      this.placeholder.classList.add('input__section-placeholder');
      this.label.classList.remove('input__label--focus');
    }
  }

  componentDidMount() {
    const { defaultValue } = this.props;

    if (defaultValue) this.handleFocus();
  }

  componentWillReceiveProps(props) {
    const { error, defaultValue } = props;

    if (error.show) {
      this.container.classList.add('input--error');
      setTimeout(() => {
        this.container.classList.remove('input--error');
      }, 2000);
    }

    if (defaultValue) this.handleFocus();
  }

  renderLabel() {
    const { label, error } = this.props;
    const settings = {
      className: `input__label input__label--${error.show ? 'error' : 'default'}`,
      ref: (e) => this.label = e
    };

    return <div {...settings}>{label}</div>;
  }

  renderError() {
    const { error } = this.props;

    return <div className={`input__error input__error--${error.show ? 'show' : 'hide'}`}>{error.message}</div>;
  }

  renderField() {
    const { type, r, error, params, defaultValue, modifiers } = this.props;
    let className = `input__field input__field-${error.show ? 'error' : 'default'}`;

    if (modifiers) {
      modifiers.map(mod => {
        className += ` input__field-${mod}`
      });
    }

    const settings = {
      type,
      className,
      inputRef: (e) => {
        this.field = e;
        return r(e);
      },
      onFocus: this.handleFocus,
      onBlur: this.handleBlur,
      disabled: modifiers && modifiers.find(mod => mod === 'disabled'),
      alwaysShowMask: false
    };

    if (defaultValue) settings.defaultValue = defaultValue;

    return <InputMask {...settings} {...params} />;
  }

  render() {
    const { error, modifiers } = this.props;

    let className = `input input--${error.show ? 'error' : 'default'}`;

    if (modifiers) {
      modifiers.map(mod => {
        className += ` input-${mod}`;
      });
    }

    return (
      <div className={className} ref={(e) => this.container = e}>
        <div className='input__section input__section-placeholder' ref={(e) => this.placeholder = e}>
          {this.renderLabel()}
        </div>
        <div className='input__section input__section-field'>
          {this.renderField()}
        </div>
        <div className='input__section input__section-error'>
          {this.renderError()}
        </div>
      </div>
    );
  }
}

export default Input;
