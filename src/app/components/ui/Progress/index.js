import './style.css';
import './widescreen.css';
import './tablet.css';
import './mobile.css';

import React from 'react';

export default ({ progress, template = 'default' }) => {
  return (
    <div className={`progress progress--${template}`}>
      <div className={`progress__bar progress__bar--${template}`} style={{ width: `${progress}%` }}/>
    </div>
  );
}