import React from "react";

export default () => {
  return (
    <svg
      className="icon icon-question"
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M5 7.29342C5 3.11954 8.33935 1.05732 11.7575 1"
        stroke="black"
        strokeWidth="1.7"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M15.5406 1.81366C17.5285 2.78577 19 4.62322 19 7.29343"
        stroke="#721FD9"
        strokeWidth="1.7"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M11.4636 22.1257V23M17.971 10.5559C15.9044 13.1845 11.4449 13.3048 11.4986 18.344L17.971 10.5559Z"
        stroke="black"
        strokeWidth="1.7"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
};
