import Angle from './Angle';
import Arrow from './Arrow';
import ArrowRound from './ArrowRound'
import Comment from './Comment';
import Edit from './Edit';
import Picture from './Picture';
import Burger from './Burger';
import Play from './Play';
import Share from './Share';
import Question from './Question';
import Download from './Download';

export default {
  angle: Angle,
  arrow: Arrow,
  arrowRound: ArrowRound,
  burger: Burger,
  comment: Comment,
  edit: Edit,
  picture: Picture,
  play: Play,
  share: Share,
  question: Question,
  download: Download
}
