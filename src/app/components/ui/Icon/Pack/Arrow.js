import React from 'react';

export default () => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" className='icon icon-arrow' viewBox="0 0 38 38">
      <path className='icon__zone' fill="none" fillRule="evenodd" stroke="#000" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M22 27.171L14 19l8-8"/>
    </svg>
  );
}
