import React from 'react';

export default () => {
  return (
    <svg className='icon icon-arrow-round' xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" viewBox="0 0 64 64">
      <defs>
          <circle id="arrow_round_b" cx="27" cy="27" r="27"/>
          <filter id="arrow_round_a" width="129.6%" height="129.6%" x="-14.8%" y="-13%" filterUnits="objectBoundingBox">
              <feOffset dy="1" in="SourceAlpha" result="shadowOffsetOuter1"/>
              <feGaussianBlur in="shadowOffsetOuter1" result="shadowBlurOuter1" stdDeviation="2.5"/>
              <feColorMatrix in="shadowBlurOuter1" result="shadowMatrixOuter1" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.2 0"/>
              <feMorphology in="SourceAlpha" radius="1" result="shadowSpreadOuter2"/>
              <feOffset dy="3" in="shadowSpreadOuter2" result="shadowOffsetOuter2"/>
              <feGaussianBlur in="shadowOffsetOuter2" result="shadowBlurOuter2" stdDeviation=".5"/>
              <feColorMatrix in="shadowBlurOuter2" result="shadowMatrixOuter2" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.12 0"/>
              <feOffset dy="2" in="SourceAlpha" result="shadowOffsetOuter3"/>
              <feGaussianBlur in="shadowOffsetOuter3" result="shadowBlurOuter3" stdDeviation="1"/>
              <feColorMatrix in="shadowBlurOuter3" result="shadowMatrixOuter3" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.14 0"/>
              <feMerge>
                  <feMergeNode in="shadowMatrixOuter1"/>
                  <feMergeNode in="shadowMatrixOuter2"/>
                  <feMergeNode in="shadowMatrixOuter3"/>
              </feMerge>
          </filter>
      </defs>
      <g fill="none" fillRule="evenodd">
          <g transform="translate(5 4)">
              <use fill="#000" filter="url(#arrow_round_a)" xlinkHref="#b"/>
              <use fill="#FFF" xlinkHref="#arrow_round_b"/>
          </g>
          <path stroke="#721FD9" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M35 39.171L27 31l8-8"/>
      </g>
    </svg>
  );
}