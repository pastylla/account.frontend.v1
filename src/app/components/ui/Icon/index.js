import "./style.css";
import "./mobile.css";
import "./tablet.css";
import "./widescreen.css";

import React from "react";

import Pack from "./Pack";

export default ({ name, template = "default", direction = "right" }) => {
  const Component = Pack[name];

  if (Component)
    return (
      <i className={`icon icon-${name} icon--${template} icon---${direction}`}>
        <Component />
      </i>
    );

  return (
    <i className={`icon icon-${name} icon--${template} icon---${direction}`} />
  );
};
