import './style.css';
import './widescreen.css';
import './tablet.css';
import './mobile.css';

import React from 'react';

export default ({ children, size="default", template="default", onClick }) => {
  return (
    <span onClick={onClick} className={`tag tag-${size} tag--${template}`}>
      {children}
    </span>
  );
}