import "./style.css";
import './mobile.css';
import './tablet.css';
import './widescreen.css';

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import Icon from '@ui/Icon';

class Button extends Component {
  static propTypes = {
    prefix: PropTypes.string,
    suffix: PropTypes.string,
    template: PropTypes.string,
    children: PropTypes.node,
    size: PropTypes.string,
    loading: PropTypes.bool,
    type: PropTypes.string,
    onClick: PropTypes.func,
    href: PropTypes.string,
    to: PropTypes.string,
    disabled: PropTypes.bool
  };

  static defaultProps = {
    template: 'default',
    size: 'default',
    loading: false,
    type: 'button',
    onClick: () => {}
  };

  renderPrefix() {
    const { prefix, template, size } = this.props;

    if (prefix) {
      return (
        <span className='button__section button__section-prefix'>
          <span className={`button__prefix button__prefix-${size} button__prefix--${template}`}>
            <Icon name={prefix} />
          </span>
        </span>
      );
    }
  }
  renderSuffix() {
    const { suffix, template, size } = this.props;

    if (suffix) {
      return (
        <span className='button__section button__section-suffix'>
          <span className={`button__suffix button__suffix-${size} button__suffix--${template}`}>
            <Icon name={suffix} />
          </span>
        </span>
      );
    }
  }

  renderContent() {
    const { children, template } = this.props;

    return (
      <span className='button__section button__section-element'>
        <span className={`button__text button__text--${template}`}>{children}</span>
      </span>
    );
  }

  renderLoader() {
    const { loading, template } = this.props;

    if (loading) {
      return (
        <span className='button__section button__section-loader'>
          <span className={`button__loader button__loader--${template}`} />
        </span>
      );
    }
  }

  render() {
    const { type, size, template, to, href, onClick, disabled } = this.props;
    const params = { className: `button button-${size} button--${template}`, disabled };

    switch (type) {
      case 'link':
        return (
          <Link to={to} {...params}>
            {this.renderPrefix()}
            {this.renderContent()}
            {this.renderSuffix()}
            {this.renderLoader()}
          </Link>
        );
      case 'external':
        return (
          <a href={href} {...params}>
            {this.renderPrefix()}
            {this.renderContent()}
            {this.renderSuffix()}
            {this.renderLoader()}
          </a>
        );
      case 'button':
      default:
        return (
          <button onClick={onClick} {...params}>
            {this.renderPrefix()}
            {this.renderContent()}
            {this.renderSuffix()}
            {this.renderLoader()}
          </button>
        );
    }
  }
}

export default Button;
