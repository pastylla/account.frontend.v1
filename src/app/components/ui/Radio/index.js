import "./style.css";
import "./mobile.css";
import "./tablet.css";
import "./widescreen.css";

import React, { Component } from "react";
import PropTypes from "prop-types";

import Tag from "@ui/Tag";
import Title from "@ui/Title";
import Text from "../Text";

class Radio extends Component {
  static propTypes = {
    id: PropTypes.string,
    items: PropTypes.array,
    onChanges: PropTypes.func
  };

  static defaultProps = {
    onChanges: () => {},
    items: [{ title: "email" }, { title: "print" }]
  };

  constructor(props) {
    super(props);
    this.state = {
      active: 0
    };
    props.onChanges(props.id, props.items[0]);
  }

  handleClick = index => {
    const { id, items, onChanges } = this.props;

    onChanges(id, items[index]);

    this.setState({ active: index });
  };

  render() {
    const { items } = this.props;
    const { active } = this.state;
    return (
      <div className="radio">
        {!!items &&
          items.map((item, index) => {
            return (
              <div
                key={`${item.title}_${index}`}
                className="radio__section radio__section-item"
              >
                <Tag
                  onClick={() => this.handleClick(index)}
                  template={active === index ? "dark-active" : "dark"}
                >
                  <Text size={18} template="white">
                    {item.title}
                  </Text>
                </Tag>
              </div>
            );
          })}
      </div>
    );
  }
}

export default Radio;
