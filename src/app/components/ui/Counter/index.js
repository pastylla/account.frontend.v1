import './style.css';
import './widescreen.css';
import './tablet.css';
import './mobile.css';

import React from 'react';

export default ({ current, length }) => {
  return (
    <div className='counter'>
      <span className='counter__current'>{current}</span> / <span className='counter__length'>{length}</span>
    </div>
  );
}