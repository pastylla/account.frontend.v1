import "./style.css";
import './mobile.css';
import './tablet.css';
import './widescreen.css';

import React from 'react';

export default ({ children }) => {
  return (
    <a className='email' href={`mailto:${children}`}>
      {children}
    </a>
  );
}