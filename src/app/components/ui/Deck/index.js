import "./style.css";
import "./tablet.css";
import "./mobile.css";
import "./widescreen.css";

import React from "react";

export default () => {
  return (
    <div className="deck">
      <div className="deck__section deck__section-card">
        <div className="deck__card deck__card--1" />
      </div>
      <div className="deck__section deck__section-card">
        <div className="deck__card deck__card--2" />
      </div>
      <div className="deck__section deck__section-card">
        <div className="deck__card deck__card--3" />
      </div>
    </div>
  );
}
