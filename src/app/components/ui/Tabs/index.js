import "./style.css";
import "./mobile.css";
import "./tablet.css";
import "./widescreen.css";

import React, { Component } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

class Tabs extends Component {
  static propTypes = {
    tabs: PropTypes.array
  };

  render() {
    const { tabs } = this.props;
    return (
      <div className="tabs">
        {tabs &&
          tabs.map(tab => {
            return (
              <div
                key={`tabs_${tab.name}`}
                className={`tabs__section tabs__section-item ${
                  tab.active ? "tabs__section--active" : ""
                }`}
              >
                <Link to={tab.to} className={`tabs__item`}>
                  {tab.name}
                </Link>
              </div>
            );
          })}
      </div>
    );
  }
}

export default Tabs;
