import "./style.css";
import "./widescreen.css";
import "./tablet.css";
import "./mobile.css";

import React, { Component } from "react";
import PropTypes from "prop-types";

import Text from "@ui/Text";
import Title from "@ui/Title";
import Link from "@ui/Link";
import Icon from "@ui/Icon";
import Button from "@ui/Button";

class Tariff extends Component {
  static propTypes = {
    date: PropTypes.string
  };

  static defaultProps = {
    name: "Lite",
    cost: "$30.35",
    date: "25 мая 2019",
    methods: [{ id: "0", name: "VISA", description: "*** 1234" }],
    refuse: () => {},
    edit: false
  };

  renderHeader() {
    const { template, name, cost, date, refuse, edit } = this.props;
    return (
      <div className="tariff__header">
        <div className="tariff__header__section tariff__header__section-title">
          <Title level={4} template="white">
            Тариф
          </Title>
        </div>
        <div className="tariff__header__section tariff__header__section-name">
          <Title level={4} template="primary">
            {name}
          </Title>
        </div>
        {edit && (
          <div className="tariff__header__section tariff__header__section-refuse">
            <Text size={16} template="white">
              Отказаться
            </Text>
          </div>
        )}
      </div>
    );
  }

  renderNext() {
    const { template, name, cost, date, edit } = this.props;
    return (
      <div className="tariff__next">
        <div className="tariff__next__section tariff__next__section-title">
          <Text size={14} template="white">
            Следующий платеж
          </Text>
        </div>
        <div className="tariff__next__section tariff__next__section-cost">
          <Title level={4} template="white">
            {cost}
          </Title>
        </div>
        <div className="tariff__next__section tariff__next__section-date">
          <Title level={4} template="white">
            {date}
          </Title>
        </div>
        {edit && (
          <div className="tariff__next__section tariff__next__section-button">
            <Button template="black">Сменить тариф</Button>
          </div>
        )}
      </div>
    );
  }

  renderMethod() {
    const { template, name, cost, date, edit, methods } = this.props;
    return (
      <div className="tariff__method">
        <div className="tariff__method__section tariff__method__section-title">
          <Text size={14} template="white">
            Способ оплаты
          </Text>
        </div>
        {methods &&
          methods.map((method, index) => {
            return (
              <div className="tariff__method__section tariff__method__section-item">
                <div className="tariff__method__item">
                  <div className="tariff__method__item__section tariff__method__item__section-name">
                    <Title level={4} template="white">
                      {method.name}
                    </Title>
                  </div>
                  <div className="tariff__method__item__section tariff__method__item__section-description">
                    <Title level={4} template="white">
                      {method.description}
                    </Title>
                  </div>
                  {edit && (
                    <div className="tariff__method__item__section tariff__method__item__section-action">
                      <Text size={16} template="white">
                        Удалить
                      </Text>
                    </div>
                  )}
                </div>
              </div>
            );
          })}

        {edit && (
          <div className="tariff__method__section tariff__method__section-button">
            <Button template="black">Добавить еще один способ оплаты</Button>
          </div>
        )}
      </div>
    );
  }

  render() {
    const { template, name, cost, date, edit } = this.props;
    return (
      <div className="tariff">
        <div className="tariff__section tariff__section-header">
          {this.renderHeader()}
        </div>
        <div className="tariff__section tariff__section-next">
          {this.renderNext()}
        </div>
        {!edit && (
          <div className="tariff__section tariff__section-history">
            <Link to="/">
              <Text size={14} template="white">
                История платежей
              </Text>
            </Link>
          </div>
        )}

        <div className="tariff__section tariff__section-method">
          {this.renderMethod()}
        </div>
      </div>
    );
  }
}

export default Tariff;
