import "./style.css";
import "./mobile.css";
import "./tablet.css";
import "./widescreen.css";

import React, { Component } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

import Icon from "@ui/Icon";

class Drop extends Component {
  static propTypes = {
    icon: PropTypes.any,
    template: PropTypes.string,
    size: PropTypes.string,
    loading: PropTypes.bool,
    type: PropTypes.string,
    onClick: PropTypes.func,
    href: PropTypes.string,
    to: PropTypes.string,
    disabled: PropTypes.bool
  };

  static defaultProps = {
    template: "default",
    size: "default",
    loading: false,
    type: "drop",
    onClick: () => {}
  };

  renderContent() {
    const { icon, template = 'default' } = this.props;
    const params = {
      name: icon.name ? icon.name : icon,
      template: icon.template ? icon.template : "default",
      direction: icon.direction ? icon.direction : "right"
    };
    return (
      <div className="drop__section drop__section-circle">
        <span className="drop__circle">
          <Icon {...params} />
        </span>
      </div>
    );
  }

  render() {
    const { type, size, template, to, href, onClick, disabled } = this.props;
    const params = {
      className: `drop drop-${size} drop--${template}`,
      disabled
    };

    switch (type) {
      case "link":
        return (
          <Link to={to} {...params}>
            {this.renderContent()}
          </Link>
        );
      case "external":
        return (
          <a href={href} {...params}>
            {this.renderContent()}
          </a>
        );
      case "button":
      default:
        return (
          <button onClick={onClick} {...params}>
            {this.renderContent()}
          </button>
        );
    }
  }
}

export default Drop;
