import "./style.css";
import "./mobile.css";
import "./tablet.css";
import "./widescreen.css";

import React, { Component } from "react";
import Slick from "react-slick";

import ProgressBar from "@ui/ProgressBar";
import Text from "@ui/Text";
import Title from "@ui/Title";

class Slider extends Component {
  constructor() {
    super();
    this.state = {
      activeSlide: 0
    };
  }
  renderSlider() {
    const { children, fade = true } = this.props;
    const params = {
      slidesToShow: fade ? 1 : 1.125,
      slidesToScroll: 1,
      autoplay: false,
      arrows: false,
      dots: false,
      infinite: false,
      autoplaySpeed: 5000,
      speed: 1000,
      fade: fade,
      ref: e => (this.slick = e),
      afterChange: current => this.setState({ activeSlide: current })
    };

    return (
      <Slick className="slider__section slider__section-slick" {...params}>
        {children.map((item, index) => {
          return (
            <div key={`slider_${index}`} className="slider__slick__section">
              {item}
            </div>
          );
        })}
      </Slick>
    );
  }

  renderProgressBar() {
    const { children } = this.props;
    const { activeSlide } = this.state;

    return (
      <div className="slider__section slider__section-progressbar">
        <ProgressBar length={children.length} active={activeSlide} />
      </div>
    );
  }

  renderInfo() {
    const title = "Выберите тему";
    const subtitle = "Какие задачи вы хотите решить с помощью презентации";

    return (
      <div className="slider__section slider__section-info">
        <div className="slider__info">
          <div className="slider__info__section slider__info__section-title">
            <Title level={5} template="white">
              {title}
            </Title>
          </div>
          <div className="slider__info__section slider__info__section-subtitle">
            <Text size={18} template="white">
              {subtitle}
            </Text>
          </div>
        </div>
      </div>
    );
  }

  render() {
    const { progressbar = true, info = true } = this.props;
    return (
      <div className="slider">
        {this.renderSlider()}
        {progressbar && (this.renderProgressBar())}
        {info && (this.renderInfo())}
      </div>
    );
  }
}

export default Slider;
