import './style.css';
import './widescreen.css';
import './tablet.css';
import './mobile.css';

import React from 'react'; 

export default ({ template = 'default', items }) => {
  return (
    <ol className={`orderedList orderedList--${template}`}>
      {items.map((item, i) => {
        return (
          <li key={`${template}_${i}_${i + 1}`} className='orderedList__item'>
            <div className='orderedList__item__section orderedList__item__section-counter'>
              <span className='orderedList__counter'>{i + 1}</span>
            </div>
            <div className='orderedList__item__section orderedList__item__section-title'>
              <span className='orderedList__title'>{item}</span>
            </div>
          </li>
        );
      })}
    </ol>
  );
}