import "./style.css";
import "./widescreen.css";
import "./tablet.css";
import "./mobile.css";

import React, { Component } from "react";
import PropTypes from "prop-types";

import Text from "@ui/Text";
import Title from "@ui/Title";
import Link from "@ui/Link";
import Icon from "@ui/Icon";

class Notification extends Component {
  static propTypes = {
    date: PropTypes.string,
    title: PropTypes.string,
    text: PropTypes.string,
    link: PropTypes.string,
    unread: PropTypes.any
  };
  render() {
    const { date, title, text, link, unread=false } = this.props;
    return (
      <div className="notification">
        <div className="notification__section notification__section-unread">
          <div
            className={`notification__unread notification__unread--${
              unread ? "active" : "false"
            }`}
          />
        </div>
        <div className="notification__section notification__section-content">
          <div className="notification__content">
            <div className="notification__content__section notification__content__section-date">
              <Text size={14} template="white">
                {date}
              </Text>
            </div>
            <div className="notification__content__section notification__content__section-title">
              <Title level={5} template="white">
                {title}
              </Title>
            </div>
            <div className="notification__content__section notification__content__section-text">
              <Text size={14} template="white">
                {text}
              </Text>
            </div>
          </div>
        </div>
        <div className="notification__section notification__section-link">
          {link && (
            <Link to={link}>
              <div className="notification__link__icon">
                <Icon name="angle" template="left"/>
              </div>
            </Link>
          )}
        </div>
      </div>
    );
  }
}

export default Notification;
