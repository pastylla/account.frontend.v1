import './style.css';
import './widescreen.css';
import './tablet.css';
import './mobile.css';

import React from 'react';

export default ({ r, onEnter, onChange, defaultValue }) => {
  const onKeyPress = (e) => {
    if (e.keyCode === 13) return onEnter();
  };

  return <textarea onChange={onChange} defaultValue={defaultValue} onKeyPress={onKeyPress} className='textarea' ref={(e) => r(e)}></textarea>;
}