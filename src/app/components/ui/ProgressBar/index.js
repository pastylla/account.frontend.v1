import "./style.css";
import "./mobile.css";
import "./tablet.css";
import "./widescreen.css";

import React from "react";

export default ({ length, active }) => {
  const array = new Array(length).fill(0);

  return (
    <div className="progress-bar">
      {array.map((item, index) => {
        return (
          <div
            key={index}
            className="progress-bar__section progress-bar__section-item"
          >
            <div
              className={`progress-bar__item progress-bar__item--${
                index === active ? "active" : "default"
              }`}
            />
          </div>
        );
      })}
    </div>
  );
};
