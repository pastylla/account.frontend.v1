import "./style.css";
import "./mobile.css";
import "./tablet.css";
import "./widescreen.css";

import React from "react";

export default ({ size=12, template = "default", children }) => {
  const params = {
    className: `text text-${size} text--${template}`
  };

  return <span {...params}>{children}</span>;
};
