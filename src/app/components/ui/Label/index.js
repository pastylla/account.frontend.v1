import "./style.css";
import "./widescreen.css";
import "./tablet.css";
import "./mobile.css";

import React, { Component } from "react";
import PropTypes from "prop-types";

import Text from "@ui/Text";

class Label extends Component {
  static propTypes = {
    title: PropTypes.string,
    additional: PropTypes.string
  };
  render() {
    const { title, children, additional } = this.props;
    return (
      <div className="label">
        {title && (
          <div className="label__section label__section-title">
            <Text size={16} template="white">
              {title}
            </Text>
          </div>
        )}
        <div className="label__section label__section-value">{children}</div>
        {!!additional && (
          <div className="label__section label__section-additional">
            <Text size={14} template="grey">
              {additional}
            </Text>
          </div>
        )}
      </div>
    );
  }
}

export default Label;
