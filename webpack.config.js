const path = require('path');

const Clean = require('clean-webpack-plugin');
const Manifest = require('webpack-manifest-plugin');
const ExtractCSS = require('mini-css-extract-plugin');
const OptimizeCSS = require("optimize-css-assets-webpack-plugin");
const UglifyJS = require("uglifyjs-webpack-plugin");

const BUILD_DIR = path.join(__dirname, './build/dist');
const DEV_DIR = path.join(__dirname, './src');

module.exports = (env, argv) => {
  const production = argv.mode === 'production';
  const plugins = [
    new ExtractCSS({
      filename: production ? 'bundle.[hash].css' : 'bundle.css'
    })
  ];

  if (production) {
    plugins.push(new Clean([
      `${BUILD_DIR}/*.js`,
      `${BUILD_DIR}/*.css`,
      `${DEV_DIR}/manifest.json`
    ]));
    plugins.push(new Manifest({
      fileName: `${DEV_DIR}/manifest.json`
    }));
  }

  return {
    entry: {
      bundle: `${DEV_DIR}/client.js`
    },
    output: {
      path: BUILD_DIR,
      filename: production ? '[name].[hash].js' : '[name].js'
    },
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          use: {
            loader: "babel-loader"
          }
        },
        {
          test: /\.css$/,
          use: [
            ExtractCSS.loader,
            {
              loader: "css-loader",
              options: {
        				importLoaders: 1,
        			}
            }
          ]
        }
      ]
    },
    optimization:  {
      minimizer: [
        new UglifyJS({
          cache: true,
          parallel: true,
          sourceMap: true
        }),
        new OptimizeCSS()
      ]
    },
    plugins,
    devServer: {
      headers: {
        'Access-Control-Allow-Origin': '*'
      },
      disableHostCheck: true,
      contentBase: './build',
      historyApiFallback: true
    }
  };
};
