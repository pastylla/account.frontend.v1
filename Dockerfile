FROM node:10.4.1-alpine

WORKDIR /app
COPY package.json /app
RUN npm install --quiet

COPY . /app
RUN npm run postinstall
RUN npm run build --quiet

ENV MODE=production
ENV PORT=2005

EXPOSE 2005

CMD ["node", "express.js"]
